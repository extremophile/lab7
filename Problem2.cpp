/***
FN:F79454
PID:2
GID:1
*/

#include <iostream>
#include <math.h>
using namespace std;

//functions here
double interest(double a, double b, double c)
{
return a*(1 + (c*b) / (100*12));
}
double compoundInterest(double a,double b, double c)
{
return pow((1 + b/100), c)*a;
}

int main()
{
cout << "Enter sum, percentage and years:" << endl;
double sum, percentage, years;
cin >> sum;
cin >> percentage;
cin >> years;
cout << "Interest: " << interest(sum,percentage,years) << endl;
cout << "Compound interest: " << compoundInterest(sum,percentage,years) << endl;
return 0;
}
