#include "stdafx.h"
#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

int main()
{
	string fnumber, fname, lname;
	int gradesCount;

	cout << fixed << setprecision(2);

	cout << setw(12) << "Faculty No." << setw(6) << "Grade" << endl;
	while (cin >> fnumber)
	{
		cin >> fname >> lname;
		cin >> gradesCount;

		double grade;
		double sum = 0;
		for (int i = 0; i<gradesCount; i++)
		{
			cin >> grade;
			sum += grade;
		}
		double avg = sum / gradesCount;

		cout << setw(12) << fnumber << setw(6) << avg << endl;
	}

	return 0;
}